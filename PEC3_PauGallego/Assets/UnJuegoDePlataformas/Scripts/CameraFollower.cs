﻿using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public Transform character;
    public Transform camera;

    private float newCamLimit;

    private void Start()
    {
        newCamLimit = 14.25f;
    }
    
    private void Update()
    {
        newCamLimit = camera.position.x;
        // Condición que impide que la cámara mueva fuera de los bordes del nivel.
        if (character.position.x > newCamLimit && character.position.x < 196.75)
        {
            // Seguimiento del personaje constante
            Vector3 newPosition = transform.position;
            newPosition.x = character.position.x;
            transform.position = newPosition;
        }
    }
}
