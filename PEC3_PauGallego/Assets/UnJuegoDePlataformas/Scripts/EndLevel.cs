using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    public GameplayManager GameplayManager;
    
    public AudioSource endSound;

    void Start()
    {
        endSound = GetComponent<AudioSource>();
    }
    
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            GameplayManager.EndLevel();
            endSound.Play();
        }
    }
}
