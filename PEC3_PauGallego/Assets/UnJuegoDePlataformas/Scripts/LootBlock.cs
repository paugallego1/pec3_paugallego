using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBlock : MonoBehaviour
{
    private SpriteRenderer block;
    public GameObject coin;
    public GameObject upgrade;
    public GameObject megaUpgrade;
    
    public AudioSource lootBoxSound;
    public AudioClip appearSound;


    public byte typeLoot;
    // 0 Coin
    // 1 Upgrade
    // 2 Mega Upgrade
    
    // Start is called before the first frame update
    void Start()
    {
        block = GetComponent<SpriteRenderer>();
        coin.SetActive(false);
        upgrade.SetActive(false);
        megaUpgrade.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            block.enabled = false;
            lootBoxSound.clip = appearSound;
            lootBoxSound.Play();
            SpawnLoot(typeLoot);
        }
    }

    private void SpawnLoot(byte type)
    {
        switch (type)
        {
            case 0:
                coin.SetActive(true);
                break;
            case 1:
                upgrade.SetActive(true);
                break;
            case 2:
                megaUpgrade.SetActive(true);
                break;
        }
    }
}
