using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class DestructibleBlock : MonoBehaviour
{
    public MainCharacter Mario;
    public BoxCollider2D collider;
    public BoxCollider2D collider2;
    public SpriteRenderer sprite;
    public ParticleSystem particle;
    
    public AudioSource desBlockSound;
    public AudioClip brokeSound;

    void Start()
    {
        collider.enabled = true;
        sprite.enabled = true;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            if (Mario.CanDestroyBlock())
            {
                StartCoroutine(NotBrokeYet());
            }
        }
    }
    IEnumerator NotBrokeYet()
    {
        yield return new WaitForSeconds(0.05f);
        desBlockSound.clip = brokeSound;
        desBlockSound.Play();
        particle.Play();
        collider.enabled = false;
        collider2.enabled = false;
        sprite.enabled = false;
    }
}
