using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacter : MonoBehaviour
{
    // Definición de objetos
    private Rigidbody2D rb;
    public GameplayManager GameplayManager;
    public Transform groundCheck;
    public Transform blockCheck;
    public LayerMask groundLayer;
    public GameObject marioUpgraded;
    public GameObject marioMegaUpgraded;
    private SpriteRenderer mario;
    public SpriteRenderer marioBig;
    public BoxCollider2D collider;
    public GameObject fireBall;
    public GameObject spawnFireBall;
    public ParticleSystem dustLeft;
    public ParticleSystem dustRight;
    public ParticleSystem dustJump;
    public ParticleSystem dustGround;

    public AudioSource marioSound;
    public AudioClip jumpSound;
    public AudioClip steepSound;
    public AudioClip upgradeSound;
    public AudioClip megaUpgradeSound;
    public AudioClip coinSound;
    public AudioClip fireBallSound;
    public AudioClip damageSound;
    public AudioClip deadSound;
    public Animator animatorMario;
    public Animator animatorMarioBig;
    public Animator animatorMarioMega;

    // Definición de variables
    public float acceleration;
    public float jumpPower;
    public float jumpPowerReaction;
    public float fallMultiplier;
    public float invulnerableDuration;
    public float launchPower;

    private float horizontal;
    private float velocity;
    private Vector2 vecGravity;
    public byte hp;
    private bool invulnerable;
    public bool grounded;
    private bool movement;
    private bool isDead;
    private bool attacking;
    public bool lookDirection;
    private bool particlePlaying;
    private bool jumped;
    

    void Start()
    {
        mario = GetComponent<SpriteRenderer>();

        rb = GetComponent<Rigidbody2D>();
        
        vecGravity = new Vector2(0, -Physics2D.gravity.y);
        
        marioStatChecker();
        invulnerable = false;
        movement = true;
        isDead = false;
        attacking = false;
        
        marioSound = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        // Movimiento horizontal del personaje principal
        horizontal = Input.GetAxisRaw("Horizontal");

        if (movement)
        {
            if (horizontal != 0.0f) {velocity = Mathf.Clamp(velocity + horizontal * acceleration * Time.deltaTime, -1.0f, 1.0f);}
            else {velocity -= velocity * acceleration * Time.deltaTime;}
        
            transform.Translate(Vector3.right * velocity * acceleration * Time.deltaTime);
        }
        
        // Animación de correr - [Horizontal se multiplica a si mismo para que siempre de su valor en positivo]
        animatorMario.SetFloat("Speed", horizontal * horizontal);
        animatorMarioBig.SetFloat("Speed", horizontal * horizontal);
        animatorMarioMega.SetFloat("Speed", horizontal * horizontal);
        
        if (horizontal == 0 || !isGrounded())
        {
            dustLeft.Stop();
            dustRight.Stop();
            particlePlaying = false;
        }

        // Dirección del personaje
        if (horizontal > 0)
        {
            flipX(false);
            lookDirection = true;
            if (isGrounded() && !particlePlaying)
            {
                dustLeft.Play();
                particlePlaying = true;
            }
        }

        if (horizontal < 0)
        {
            flipX(true);
            lookDirection = false;
            if (isGrounded() && !particlePlaying)
            {
                dustRight.Play();
                particlePlaying = true;
            }
        }
        
        if (!lookDirection)
        {
            spawnFireBall.transform.rotation = Quaternion.Euler(0, 0, 180);
        }
        else
        {
            spawnFireBall.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        // Salto del personaje principal
        grounded = isGrounded();
        
        if (isGrounded() && !jumped)
        {
            dustGround.Play();
            jumped = true;
        }
        if (isGrounded())
        {
            TimeInAir();

        }
        
        if (Input.GetButtonDown("Jump") && isGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
            animatorMario.SetBool("Grounded", grounded);
            animatorMarioBig.SetBool("Grounded", grounded);
            animatorMarioMega.SetBool("Grounded", grounded);
            marioSound.clip = jumpSound;
            marioSound.Play();
            dustJump.Play();
        }
        if (rb.velocity.y < 0) {rb.velocity -= vecGravity * fallMultiplier * Time.deltaTime;}
        
        // Disparo del Mega Mario
        if (Input.GetKeyDown(KeyCode.X) && hp == 3)
        {
            ShootFireBall();
            StartCoroutine(ShootAni());
            attacking = true;
        }
        
        // Animaciones
        
        // Animación de muerte
        animatorMario.SetBool("Dead", isDead);

        // Animación de salto
        animatorMario.SetBool("Grounded", grounded);
        animatorMarioBig.SetBool("Grounded", grounded);
        animatorMarioMega.SetBool("Grounded", grounded);
        
        // Animación de disparo
        animatorMarioMega.SetBool("Attacking", attacking);
    }

    IEnumerator ShootAni()
    {
        attacking = true;
        yield return new WaitForSeconds(0.25f);
        attacking = false;
    }
    
    IEnumerator TimeInAir()
    {
        jumped = true;
        yield return new WaitForSeconds(0.5f);
        jumped = false;
    }
    public bool isGrounded()
    {
        return Physics2D.OverlapCapsule(groundCheck.position, new Vector2(0.4f, 0.15f), CapsuleDirection2D.Horizontal, 0, groundLayer);
    }
    
    public bool CanDestroyBlock()
    {
        return Physics2D.OverlapCapsule(blockCheck.position, new Vector2(0.4f, 0.15f), CapsuleDirection2D.Horizontal, 0, groundLayer);
    }
    
    public void flipX(bool flip)
    {
        Vector3 scale = transform.localScale;
        scale.x = flip ? Mathf.Abs(scale.x) * -1f : Mathf.Abs(scale.x);
        transform.localScale = scale;
    }

    public void ShootFireBall()
    {
        GameObject fireBallIns = Instantiate(fireBall, spawnFireBall.transform.position, spawnFireBall.transform.rotation);

        fireBallIns.GetComponent<Rigidbody2D>().AddForce(transform.right * launchPower);
        
        marioSound.clip = fireBallSound;
        marioSound.Play();
    }

    public void marioStatChecker()
    {
        switch (hp)
        {
            case 0:
                dead();
                break;
            case 1:
                noUpgrade();
                break;
            case 2:
                marioUpgrade();
                break;
            case 3:
                marioMegaUpgrade();
                break;
        }
    }
    public void noUpgrade()
    {
        collider.enabled = true;
        mario.enabled = true;
        marioUpgraded.SetActive(false);
        marioBig.enabled = false;
        marioMegaUpgraded.SetActive(false);
    }
    public void marioUpgrade()
    {
        collider.enabled = false;
        mario.enabled = false;
        marioUpgraded.SetActive(true);
        marioBig.enabled = true;
        marioMegaUpgraded.SetActive(false);
        marioSound.clip = upgradeSound;
        marioSound.Play();
    }
    public void marioMegaUpgrade()
    {
        collider.enabled = false;
        mario.enabled = false;
        marioUpgraded.SetActive(true);
        marioBig.enabled = false;
        marioMegaUpgraded.SetActive(true);
        marioSound.clip = megaUpgradeSound;
        marioSound.Play();
    }
    public void dead()
    {
        collider.enabled = false;
        mario.enabled = true;
        marioUpgraded.SetActive(false);
        marioBig.enabled = false;
        marioMegaUpgraded.SetActive(false);
        StartCoroutine(TimerToSpawn());
    }
    IEnumerator TimerToSpawn()
    {
        isDead = true;
        marioSound.clip = deadSound;
        marioSound.Play();
        //mario.enabled = false;
        rb.velocity = new Vector2(rb.velocity.x, jumpPowerReaction);
        movement = false;
        yield return new WaitForSeconds(3f);
        GameplayManager.RestartLevel();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (invulnerable == false)
            {
                if (isGrounded())
                {
                    hp--;
                    marioStatChecker();
                    if (hp > 0)
                    {
                        marioSound.clip = damageSound;
                        marioSound.Play();
                        StartCoroutine(invulnerableOn());
                    }
                }
                else
                {
                    marioSound.clip = steepSound;
                    marioSound.Play();
                    rb.velocity = new Vector2(rb.velocity.x, jumpPowerReaction);
                }
            }
        }
        IEnumerator invulnerableOn()
        {
            invulnerable = true;
            yield return new WaitForSeconds(invulnerableDuration);
            invulnerable = false;
        }

        if (collision.CompareTag("DeadZone"))
        {
            hp = 0;
            marioStatChecker();
        }
        
        if (collision.CompareTag("Upgrade"))
        {
            hp = 2;
            marioStatChecker();
        }
        
        if (collision.CompareTag("MegaUpgrade"))
        {
            hp = 3;
            marioStatChecker();
        }

        if (collision.CompareTag("Coin"))
        {
            marioSound.clip = coinSound;
            marioSound.Play();
        }

        if (collision.CompareTag("End"))
        {
            movement = false;
        }
    }
}
