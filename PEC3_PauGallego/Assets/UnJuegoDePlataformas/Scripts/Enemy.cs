using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameplayManager GameplayManager;
    public MainCharacter Mario;
    private Rigidbody2D rb;
    public BoxCollider2D collider;
    public Animator animator;
    public ParticleSystem dust;

    public GameObject colliderLive;
    public GameObject colliderDead;

    public float speed;
    public bool direction;
    private bool dead = false;

    void Start()
    {
        dead = false;
        colliderDead.SetActive(false);
        colliderLive.SetActive(true);
    }

    void Update()
    {
        if (!dead)
        {
            if (direction)
            {
                transform.Translate(Vector2.right * speed * Time.deltaTime);
            }
            else
            {
                transform.Translate(Vector2.left * speed * Time.deltaTime);
            }
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter") && !Mario.isGrounded() || collision.CompareTag("FireBall"))
        {
            dead = !dead;
            animator.SetBool("Dead", dead);
            collider.enabled = false;
            colliderDead.SetActive(true);
            colliderLive.SetActive(false);
            GameplayManager.points = GameplayManager.points + 100;
            dust.Play();
            StartCoroutine(NotDeadYet());
        }
        
        if (collision.CompareTag("Limit")) {direction = !direction;}
    }
    
    IEnumerator NotDeadYet()
    {
        yield return new WaitForSeconds(0.25f);
        Destroy(gameObject);
    }
}
