using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    private Rigidbody2D rb;
    public MainCharacter MainCharacter;
    public float speed;

    void Update()
    {
        rb = GetComponent<Rigidbody2D>();
        SetStraightVelocity();
    }

    private void SetStraightVelocity()
    {
        rb.velocity = transform.right * speed;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(NotBrokeYet());
    }
    IEnumerator NotBrokeYet()
    {
        yield return new WaitForSeconds(0.05f);
        Destroy(gameObject);
    }
}
