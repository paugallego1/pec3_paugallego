using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public GameplayManager GameplayManager;
    
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            GameplayManager.points = GameplayManager.points + 50;
            Destroy(gameObject);
        }
    }
}
