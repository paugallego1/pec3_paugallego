﻿using System.Net.Sockets;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    private static float TIME_INIT = 500;
    
    public MainCharacter MainCharacter;
    public Text timer;
    public Text pointCounter;
    public Text timerMenu;
    public Text pointCounterMenu;
    public GameObject UI;
    public GameObject Menu;

    public float time;
    public float timeFinal;
    public int points;
    private bool stopTime;

    void Start()
    {
        MainCharacter.enabled = true;
        stopTime = false;
        time = TIME_INIT;
        points = 0;
        UI.SetActive(true);
        Menu.SetActive(false);
    }

    void Update()
    {
        timer.text = "" + time.ToString("f0");
        
        pointCounter.text = "" + points.ToString();

        if (!stopTime)
        {
            time -= Time.deltaTime;
        }
        
        if (time < 0)
        {
            RestartLevel();
        }
    }

    public void EndLevel()
    {
        UI.SetActive(false);
        Menu.SetActive(true);
        stopTime = true;
        timeFinal = TIME_INIT - time;
        timerMenu.text = "" + timeFinal.ToString("f0");
        pointCounterMenu.text = "" + points.ToString();
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
