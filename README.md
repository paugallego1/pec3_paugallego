# Documento del PEC3: Un juego de plataformas de Pau Gallego

[JUGAR JUEGO AQUÍ](https://ghalek.itch.io/pec3-paugallego)

# IMPORTANTE ANTES DE JUGAR

Para lanzar el proyectil se necesita ser Mega Mario (Mario grande y blanco) y se lanza proyectiles con la tecla X.

## Descripción del juego

El juego es una recreación del nivel 1.1 de Super Mario Bross. Al igual que en el juego original; el jugador controla a Mario el cual solo puede desplazarse horizontalmente y saltar y para completar los niveles deberá alcanzar al final de los mismos. Hay obstáculos y enemigos que matan de manera instantánea a Mario pero Mario puede obtener una mejora la cúal le permite ser más grande y resistir un golpe más.

# Desarrollo del juego

## Creación del personaje principal (Mario)

Para programar al Mario, primero se ha realizado el movimiento básico horizontal. Utilizando los inputs asociado al movimiento horizontal que trae por defecto Unity (A, D y las flechas horizontales del teclado) el jugador puede desplazarse hacia la derecha o la izquierda. Dentro de la función Update se ha asociado una variable a dicho input para determinar hacia que dirección, del eje de la x, se desplaza el personaje. Este valor se multiplica con otra variable que determina qué tan rápido es Mario.

```
    horizontal = Input.GetAxisRaw("Horizontal");

    if (horizontal != 0.0f) {velocity = Mathf.Clamp(velocity + horizontal * acceleration * Time.deltaTime, -1.0f, 1.0f);}
     else {velocity -= velocity * acceleration * Time.deltaTime;}
        
    transform.Translate(Vector3.right * velocity * acceleration * Time.deltaTime);
```

También se ha tenido en cuenta que el sprite del personaje mire en la dirección en la que corre. Para ello se hace una función que escala por el mismo valor pero en negativo dependiendo de si el valor de horizontal es negativo o positivo.

```
    if (horizontal > 0) {flipX(false);}
    if (horizontal < 0) {flipX(true);}

    public void flipX(bool flip)
    {
        Vector3 scale = transform.localScale;
        scale.x = flip ? Mathf.Abs(scale.x) * -1f : Mathf.Abs(scale.x);
        transform.localScale = scale;
    }
```

Para el salto se aplica una fuerza hacia arriba al utilizar el input de salto (Barra espaciadora) y con la condición de que Mario toque el suelo. La condición se ha creado para evitar que Mario pueda saltar de manera indefinida y volar. Se ha creado una cápsula en los pies del personaje que comprueba si toca el suelo o no. Eso también se hace para evitar que Mario pueda escalar superficies horizontales ya que si se usara el propio collider del personaje, el jugador podría saltar de manera indefinida si está continuamente tocando una pared aunque no toque el suelo.

```
    if (Input.GetButtonDown("Jump") && isGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
        }

    if (rb.velocity.y < 0) {rb.velocity -= vecGravity * fallMultiplier * Time.deltaTime;}

    bool isGrounded()
    {
        return Physics2D.OverlapCapsule(groundCheck.position, new Vector2(0.8f, 0.15f), CapsuleDirection2D.Horizontal, 0, groundLayer);
    }
```

También se han creado los 3 estados posibles del personaje; normal o pequeño, muerto y mejorado o grande. El pequeño o normal es el estado inicial de Mario. En este estado, Mario es vulnerable y basta de un solo ataque enemigo para morir. Grande o mejorado es el estado en el que Mario es un poco más poderoso. Cambia su sprite por una versión más grande y detallada, puede resistir un golpe de los enemigos aunque al hacerlo vuelve a su estado pequeño y además puede romper los bloques de ladrillo si los golpea con la cabeza al saltar.

Para representar al Mario grande, se ha creado un gameobject hija del Mario normal el cúal contiene el sprite de Mario grande y un collider más grande. Cuando el MArio grande se activa, se activa también el gameobject del grande pero se desactiva el sprite del pequeño. Como la colisión del pequeño queda dentro de la colisión del grande, no es necesario sustituirla. Para obtener esta mejora, hay que chocar con la seta del principio.

Para el estado de muerte simplemente se reinicia el nivel.

```
    public void noUpgrade()
    {
        marioUpgraded.SetActive(false);
        mario.enabled = true;
    }
    public void marioUpgrade()
    {
        marioUpgraded.SetActive(true);
        mario.enabled = false;
    }
    public void dead()
    {
        GameplayManager.RestartLevel();
    }
```

Para controlar los estados, se ha creado un switch para determinar en qué estado debe estar Mario dependiendo de sus "hit points" o hp. Este switch está dentro de una función que el programa llama cada vez que Mario colisiona con un enemigo o la seta y así evitar tenerla en el Update consumiendo memoria. También se ha creado un estado de "invulnerabilidad" de Mario para que el jugador pueda eliminar a los enemigos sin recibir daño. Esta estado se activa cuando el jugador está en el aire ya que para derrotar a los enemigos hay que caer encima de ellos. Tanto a la seta como al enemigo se le aplica un Destroy al colisionar.

Para reconocer si la colisión es de un enemigo o la seta, se ha utilizado las etiquetas.

```
    public void marioStatChecker()
    {
        switch (hp)
        {
            case 0:
                dead();
                break;
            case 1:
                noUpgrade();
                break;
            case 2:
                marioUpgrade();
                break;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (invulnerable == false)
            {
                if (isGrounded())
                {
                    hp--;
                    marioStatChecker();
                    StartCoroutine(invulnerableOn());
                }
            }
        }      
        if (collision.CompareTag("Upgrade"))
        {
            hp = 2;
            marioStatChecker();
        }
    }
```

## Seguimiento de la cámara

Para crear el seguimiento de la cámara con el personaje se ha programado siguiendo una lógica consistente en avanzar al mismo nivel que personaje principal, siguiendo su misma posición. Cuando el personaje gira hacia atrás, osea la derecha, la cámara se queda fija y no retrocede como ocurre en el juego original. También hay una barrera en el borde derecho de la pantalla para evitar que el personaje escape de la cámara cuando vaya hacia la derecha.

```
    newCamLimit = camera.position.x;

    if (character.position.x > newCamLimit && character.position.x < 196.75)
        {
            Vector3 newPosition = transform.position;
            newPosition.x = character.position.x;
            transform.position = newPosition;
        }
```
Los límites están delimitados físicamente con colisiones dentro del nivel.

## Creación del enemigo (Gommpa)

El enemigo se ha programado de manera que esté siempre moviéndose horizontalmente en un estado constante de patrulla. Con unos colliders invisibles etiquetados como "Limit" hacen que en caso de que el Goompa colisione con uno, cambie de dirección.

```
    void Update()
    {
        if (direction)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            Destroy(gameObject);
        }
        if (collision.CompareTag("Limit")) {direction = !direction;}
    }
```

## Creación de los bloques sorpresa

Para el bloque sorpresa se ha programado para que si el jugador golpea dicho bloque desde abajo aparezca un objeto determinado: Una moneda, el power-up o el power-up mejorado.

```
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            if (Mario.CanDestroyBlock())
            {
                StartCoroutine(NotBrokeYet());
            }
        }
    }
```
Para que el bloque se destruya solo cuando Mario lo golpea por debajo, se ha creado un collider específico en la parte superior del personaje. Un caso similar al collider que comprueba si Mario está tocando el suelo para determinar si puede saltar o no.

```
    public bool CanDestroyBlock()
    {
        return Physics2D.OverlapCapsule(blockCheck.position, new Vector2(0.4f, 0.15f), CapsuleDirection2D.Horizontal, 0, groundLayer);
    }
```

## Creación del proyectil

Para el proyectil se ha creado un "Prefab" que contiene dicho objeto. Este objeto posee una colisión que al colisionar con los objetos con la etiqueta "enemigo" los eliminará. Dentro del script del personaje, se ha creado una función que crea una instancia de este objeto para que se pueda multiplicar y ser lanzado con múltiples veces.

```
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) && hp == 3)
        {
            ShootFireBall();
            StartCoroutine(ShootAni());
            attacking = true;
        }
    }

    public void ShootFireBall()
    {
        GameObject fireBallIns = Instantiate(fireBall, spawnFireBall.transform.position, spawnFireBall.transform.rotation);

        fireBallIns.GetComponent<Rigidbody2D>().AddForce(transform.right * launchPower);
        
        marioSound.clip = fireBallSound;
        marioSound.Play();
    }
```

## creación de la UI

Se ha implementado una UI con una cuenta atrás y un contador de puntos dependiendo de las monedas recogidas y los enemigos eliminados.

También se ha creado una pantalla adicional cuando alcanzas el final el cúal te imprime el tiempo que has tardado en completar el nivel junto con tu total de puntos. También aparecen 2 botones que permiten reiniciar el nivel o salir del juego.

```
    void Update()
    {
        timer.text = "" + time.ToString("f0");
        
        pointCounter.text = "" + points.ToString();

        if (!stopTime)
        {
            time -= Time.deltaTime;
        }
        
        if (time < 0)
        {
            RestartLevel();
        }
    }

    public void EndLevel()
    {
        UI.SetActive(false);
        Menu.SetActive(true);
        stopTime = true;
        timeFinal = TIME_INIT - time;
        timerMenu.text = "" + timeFinal.ToString("f0");
        pointCounterMenu.text = "" + points.ToString();
    }
```

## Creación e implementación de las animaciones

Se han implementado varias animaciones, tanto para los personajes como para objetos. Para Mario se han creado las animaciones de idle, correr, saltar y muerte. También se han añadido estas animaciones a las versiones mejoradas de Mario además de la animación de lanzar proyectil. Para cambiar las animaciones durante el juego se realizan mediante una máquina de estados o un "animator" de Unity. Este comprueba los parámetros de las variables para averiguar si Mario está corriendo, quieto, saltando o muerto y ejecuta la animación en consecuencia. Lo mismo ocurre con el Goomba pero en el caso del Goomba solo posee dos animaciones, el de movimiento y muerte.
 
Para las animaciones de los objetos, tales como el parpadeo la caja sorpresa o el de la moneda y el proyectil no se ha utilizado ninguna máquina de estados, pues solo tienen esa animación. Simplemente cuando se activa la caja sorpresa o se recoge la moneda estas desaparecen con un "Destroy" o con quitando el renderizado del sprite.

## Implementación de sonidos

Para implementar los sonidos se ha hecho uso de AudioSource y AudioClip dentro del código para que se reproduzcan efectos de sonidos después de un evento o acción. Por ejemplo tras saltar:

```
    if (Input.GetButtonDown("Jump") && isGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
            animatorMario.SetBool("Grounded", grounded);
            animatorMarioBig.SetBool("Grounded", grounded);
            animatorMarioMega.SetBool("Grounded", grounded);

            marioSound.clip = jumpSound; // Aplica el clip de audio al AudioSource
            marioSound.Play(); // Activa el AudioSource

            dustJump.Play();
        }
```

## Creación de partículas

Se han creado varias partículas; Humo/polvo cuando Mario corre y salta, polvo cuando un Goomba muere y trozos de bloque cuando uno se destruye. Para implementarlos es muy similar con los sonidos. Se reproducen tras alguna acción. Por ejemplo en el bloque:

```
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            if (Mario.CanDestroyBlock())
            {
                StartCoroutine(NotBrokeYet());
            }
        }
    }
    IEnumerator NotBrokeYet()
    {
        yield return new WaitForSeconds(0.05f);
        desBlockSound.clip = brokeSound;
        desBlockSound.Play();

        particle.Play(); // Activa el sistema de partículas

        collider.enabled = false;
        collider2.enabled = false;
        sprite.enabled = false;
    }
```

## Sprites

Para implementar los sprites del escenario se ha utilizado una plantilla descargada de una web de recursos de videojuegos clásicos. Para los sprites de Mario, el Goompa y la seta se han recreado con Aseprite, un software de creación de sprites e imágenes con pixel art con una interfaz y uso similar a Adobe Photoshop.

## Recursos utilizados

- [ ] Sprites de Mario, Goompa y seta: Recreación propia.
- [ ] Sprites de elementos del nivel: [Super Mario Bros](https://www.spriters-resource.com/nes/supermariobros/) de [Spriters resource](https://www.spriters-resource.com).
- [ ] Efectos de sonido: [Super Mario Bros (NES)](https://themushroomkingdom.net/media/smb/wav) de [The Mushroom Kingdom](https://themushroomkingdom.net).
- [ ] Musica de fondo: [Super Mario Bros (NES) Music - Overworld Theme](https://www.youtube.com/watch?v=iy3qq7zc4EY) de [GBelair](https://www.youtube.com/@GBelair).
- [ ] Fuente de texto: [Pixeloid Font Family](https://www.1001fonts.com/pixeloid-font.html) de [GGBotNet](https://www.1001fonts.com/users/GGBotNet/).